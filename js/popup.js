let showTableBtn = document.getElementById("btnShowTable");
let clearTimeBtn = document.getElementById("btnClearTimes");
let errorMessage = document.getElementById("errorMessage");
let timeTable = document.getElementById("timeTable");
let domain;
let likeButton = document.getElementById("likeButton");
let loginButton = document.getElementById("btnLogin");
let hash = document.getElementById("hashtag");

likeButton.addEventListener("click", toggle);

function toggle() {
  chrome.storage.sync.get("tabTimesObject", function(dataCont) {
    let dataString = dataCont["tabTimesObject"];
    if (dataString == null) {
      return;
    }

    try {
      let data = JSON.parse(dataString);
      let entries = [];

      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          entries.push(data[key]);
        }
      }

      function _getCurrentTab(extractHostname) {
        chrome.tabs.query(
          {
            active: true,
            currentWindow: true
          },
          function callback(tabs) {
            var tab_domain = document.getElementById("tab_domain");
            var tab_favicon = document.getElementById("tab_favicon");

            var favicon = tabs[0].favIconUrl;
            if (favicon === undefined) {
              favicon = "chrome://favicon/" + domain;
            }
            let url = extractHostname(tabs[0].url);
            tab_domain.textContent = url;
            tab_favicon.src = favicon;
          }
        );
      }
      _getCurrentTab(extractHostname);

      function extractHostname(url) {
        var hostname;

        if (url.indexOf("//") > -1) {
          hostname = url.split("/")[2];
        } else {
          hostname = url.split("/")[0];
        }

        hostname = hostname.split(":")[0];
        hostname = hostname.split("?")[0];

        for (i = 0; i < entries.length; i++) {
          //console.log(entries[i].url)
          if (entries[i].url == hostname) {
           // console.log(entries[i].liked);
            let val = entries[i].liked;
            let hashTag = entries[i].hashtag;
            if (val) {
              chrome.runtime.sendMessage({
                popup: true,
                saveState: !val,
                hashtag: hashTag
              });
             // console.log("message send" + !val);
              likeButton.style.cssText =
                " background-position: 2800px 0; transition: background 1s steps(-28); background: url(https://cssanimation.rocks/images/posts/steps/heart.png) no-repeat;";
            } else {
              chrome.runtime.sendMessage({
                popup: true,
                saveState: !val,
                hashtag: hashTag
              });
            //  console.log("message send" + !val);
              likeButton.style.cssText =
                " background-position: -2800px 0; transition: background 1s steps(28);";
            }

            break;
          }
        }
        return hostname;
      }
    } catch (e) {
      const message = "loading the timeObject went wrong " + e.toString();
      //console.log(message);
    }
  });
}

chrome.storage.sync.get("tabTimesObject", function(dataCont) {
  let dataString = dataCont["tabTimesObject"];
  if (dataString == null) {
    return;
  }
  try {
    let data = JSON.parse(dataString);

    var rowCount = timeTable.rows.length;
    for (var x = rowCount - 1; x >= 0; x--) {
      timeTable.deleteRow(x);
    }

    let entries = [];

    for (var key in data) {
      if (data.hasOwnProperty(key)) {
        entries.push(data[key]);
      }
    }

    function _getCurrentTab(extractHostname) {
      chrome.tabs.query(
        {
          active: true,
          currentWindow: true
        },
        function callback(tabs) {
          var tab_domain = document.getElementById("tab_domain");
          var tab_favicon = document.getElementById("tab_favicon");

          var favicon = tabs[0].favIconUrl;
          if (favicon === undefined) {
            favicon = "chrome://favicon/" + domain;
          }
          let url = extractHostname(tabs[0].url);
          tab_domain.textContent = url;
          tab_favicon.src = favicon;
        }
      );
    }

    _getCurrentTab(extractHostname);

    function extractHostname(url) {
      var hostname;

      if (url.indexOf("//") > -1) {
        hostname = url.split("/")[2];
      } else {
        hostname = url.split("/")[0];
      }

      hostname = hostname.split(":")[0];
      hostname = hostname.split("?")[0];

      for (i = 0; i < entries.length; i++) {
        //console.log(entries[i].url)
        if (entries[i].url == hostname) {
          var total_time = document.getElementById("total_time");
          console.log(entries[i].url);
          let time_ =
            entries[i].trackedSeconds != null ? entries[i].trackedSeconds : 0;

          let timer = secondsToHms(time_);

          function secondsToHms(d) {
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor((d % 3600) / 60);
            var s = Math.floor((d % 3600) % 60);

            var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
            var mDisplay =
              m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
            var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
            console.log(hDisplay + " " + mDisplay + " " + sDisplay);
            return hDisplay + mDisplay + sDisplay;
          }
          total_time.textContent = "Time Spend on this Domain : " + timer;

          var val = entries[i].liked;
          var tab_hashtag = document.getElementById("hashtag");
          tab_hashtag.value = entries[i].hashtag;

          if (val) {
            likeButton.style.cssText =
              "background-position: -2800px 0; transition: background 1s steps(28);";
          } else {
            likeButton.style.cssText =
              "background-position: 2800px 0; transition: background 1s steps(-28); background: url(https://cssanimation.rocks/images/posts/steps/heart.png) no-repeat;";
          }

          break;
        }
      }
      return hostname;
    }
  } catch (e) {
    const message = "loading the timeObject went wrong " + e.toString();
    console.log(message);

    errorMessage.innerHTML = message;
    errorMessage.innerHTML = dataString;
  }
});

document.addEventListener("DOMContentLoaded", function() {
  let button = document.getElementById("btnShowInsight");
  button.addEventListener("click", function() {
    button.addEventListener("click", function() {
      chrome.tabs.create({
        url: "https://toolmark.io/"
      });
    });
  });
  function cookieinfo() {
    chrome.cookies.getAll({}, function(cookie) {
      console.log(cookie.length);
      let status = false;
      for (i = 0; i < cookie.length; i++) {
        if (cookie[i].domain === "toolmark.io") {
          console.log(JSON.stringify(cookie[i]));
          status = true;
          break;
        }
      }
      if (!status) {
        // button.addEventListener("click", function() {
        //   chrome.runtime.sendMessage({
        //     searchAllInsight: true
        //   });
        // });
        const a = document.createElement("a");
        a.id = "link";
        a.target = "_blank";
        a.classList.add("button", "button_login");
        a.innerHTML = "Login";
        a.addEventListener("click", function() {
          chrome.tabs.create({
            url: "https://toolmark.io/"
          });
        });
        const h1 = document.createElement("h1");
        h1.innerHTML = "Save Your #Bookmark Over Cloud";
        h1.classList.add("h1");
        document.getElementById("login-container").appendChild(h1);
        document.getElementById("login-container").appendChild(a);
      } else {
        button.addEventListener("click", function() {
          chrome.tabs.create({
            url: "https://toolmark.io/"
          });
        });
      }
    });
  }
  cookieinfo();
});

hash.onblur = function() {
  let value = hash.value;
  console.log("blured" + value);
  chrome.storage.sync.get("tabTimesObject", function(dataCont) {
    let dataString = dataCont["tabTimesObject"];
    if (dataString == null) {
      return;
    }

    try {
      let data = JSON.parse(dataString);
      let entries = [];

      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          entries.push(data[key]);
        }
      }

      function _getCurrentTab(extractHostname) {
        chrome.tabs.query(
          {
            active: true,
            currentWindow: true
          },
          function callback(tabs) {
            var tab_domain = document.getElementById("tab_domain");
            var tab_favicon = document.getElementById("tab_favicon");

            var favicon = tabs[0].favIconUrl;
            if (favicon === undefined) {
              favicon = "chrome://favicon/" + domain;
            }
            let url = extractHostname(tabs[0].url);
            tab_domain.textContent = url;
            tab_favicon.src = favicon;
          }
        );
      }
      _getCurrentTab(extractHostname);

      function extractHostname(url) {
        var hostname;

        if (url.indexOf("//") > -1) {
          hostname = url.split("/")[2];
        } else {
          hostname = url.split("/")[0];
        }

        hostname = hostname.split(":")[0];
        hostname = hostname.split("?")[0];

        for (i = 0; i < entries.length; i++) {
          //console.log(entries[i].url)
          if (entries[i].url == hostname) {
            console.log(entries[i].liked);
            let val = entries[i].liked;
            chrome.runtime.sendMessage({
              popup: true,
              saveState: val,
              hashtag: value
            });

            break;
          }
        }
        return hostname;
      }
    } catch (e) {
      const message = "loading the timeObject went wrong " + e.toString();
      console.log(message);
    }
  });
};

//test

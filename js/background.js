const tabTimeObjectKey = "tabTimesObject";
const lastActiveTabKey = "lastActiveTab";

chrome.runtime.onInstalled.addListener(function() {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([
      {
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: {}
          })
        ],
        actions: [new chrome.declarativeContent.ShowPageAction()]
      }
    ]);
  });
});

chrome.windows.onFocusChanged.addListener(function(windowId) {
  if (windowId == chrome.windows.WINDOW_ID_NONE) {
    processTabChange(false);
  } else {
    processTabChange(true);
  }
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request.popup) {
    //console.log("message received" + request.saveState);
    chrome.tabs.query(
      {
        active: true
      },
      function(tabs) {
        if (tabs.length > 0 && tabs[0] != null) {
          let currentTab = tabs[0];
          let url = currentTab.url;
          let title = currentTab.title;
          let hostName = url;

          try {
            let urlObject = new URL(url);
            hostName = urlObject.hostname;
          } catch (e) {
            console.log(
              `could not construct url from ${currentTab.url}, error${e}`
            );
          }

          var favicon = tabs[0].favIconUrl;
          if (favicon === undefined) {
            favicon = "chrome://favicon/" + hostName;
          }

          chrome.storage.sync.get(
            [tabTimeObjectKey, lastActiveTabKey],
            function(result) {
              let lastActiveTabString = result[lastActiveTabKey];
              let tabTimeObjectString = result[tabTimeObjectKey];

             // console.log("backgroundjs,getresult");

              //console.log(result);
              tabTimeObject = {};

              if (tabTimeObjectString != null) {
                try {
                  tabTimeObject = JSON.parse(tabTimeObjectString);
                } catch (e) {
                  console.log("exception" + e);
                }
              }

              lastActiveTab = {};
              if (lastActiveTabString != null) {
                try {
                  lastActiveTab = JSON.parse(lastActiveTabString);
                } catch (e) {
                  console.log("exception" + e);
                }
              }

              if (
                lastActiveTab.hasOwnProperty("url") &&
                lastActiveTab.hasOwnProperty("lastDateVal")
              ) {
                let lastUrl = lastActiveTab["url"];
                let currentDateVal_ = Date.now();
                let passedSecond =
                  (currentDateVal_ - lastActiveTab["lastDateVal"]) * 0.001;

                if (tabTimeObject.hasOwnProperty(lastUrl)) {
                  let lastUrlObjectInfo = tabTimeObject[lastUrl];

                  if (lastUrlObjectInfo.hasOwnProperty("trackedSeconds")) {
                    lastUrlObjectInfo["trackedSeconds"] =
                      lastUrlObjectInfo["trackedSeconds"] + passedSecond;
                  } else {
                    lastUrlObjectInfo["trackedSeconds"] = passedSecond;
                  }

                  if (lastUrlObjectInfo.hasOwnProperty("liked")) {
                    lastUrlObjectInfo["liked"] = request.saveState;
                  }

                  if (lastUrlObjectInfo.hasOwnProperty("hashtag")) {
                    console.log(request.hashtag);
                    lastUrlObjectInfo["hashtag"] = request.hashtag;
                  }

                  if (lastUrlObjectInfo.hasOwnProperty("favicon")) {
                    // console.log(request.hashtag)
                    lastUrlObjectInfo["favicon"] = favicon;
                  }
                  // lastUrlObjectInfo["favicon"] = favicon;
                  lastUrlObjectInfo["lastDateVal"] = currentDateVal_;
                } else {
                  let newUrlInfo = {
                    favicon: lastUrlObjectInfo["favicon"],
                    url: lastUrl,
                    trackedSeconds: passedSecond,
                    lastDateVal: currentDateVal_,
                    hashtag: "",
                    liked: false
                  };
                  tabTimeObject[lastUrl] = newUrlInfo;
                }
              }

              let currentDateVal = Date.now();
              let lastTabInfo = {
                url: hostName,
                lastDateVal: currentDateVal
              };

              let newLastTabObject = {};
              newLastTabObject[lastActiveTabKey] = JSON.stringify(lastTabInfo);

              chrome.storage.sync.set(newLastTabObject, function() {
               // console.log("lastActiveTab stored:" + hostName);

                const tabTimeObjectString = JSON.stringify(tabTimeObject);
                let newTabTimeObject = {};

                var req = new XMLHttpRequest();
                const baseUrl = "https://toolmark.io/userInfoUpdate";

                req.open("POST", baseUrl, true);
                req.setRequestHeader("Content-Type", "application/json");
                req.send(tabTimeObjectString);

                req.onreadystatechange = function() {
                  // Call a function when the state changes.
                  if (
                    this.readyState === XMLHttpRequest.DONE &&
                    this.status === 200
                  ) {
                    //console.log("Got response 200!");
                  }
                };

                newTabTimeObject[tabTimeObjectKey] = tabTimeObjectString;
                chrome.storage.sync.set(newTabTimeObject, function() {});
              });
            }
          );
        }
      }
    );
  }
});

function processTabChange(isWindowActive) {
  chrome.tabs.query(
    {
      active: true
    },
    function(tabs) {
     // console.log("isWindowActive :" + isWindowActive);
      //console.log(tabs);

      if (tabs.length > 0 && tabs[0] != null) {
        let currentTab = tabs[0];
        let url = currentTab.url;
        let title = currentTab.title;
        let hostName = url;

        try {
          let urlObject = new URL(url);
          hostName = urlObject.hostname;
        } catch (e) {
          console.log(
            `could not construct url from ${currentTab.url}, error${e}`
          );
        }

        var favicon = tabs[0].favIconUrl;
        if (favicon === undefined) {
          favicon = "chrome://favicon/" + hostName;
        }

        chrome.storage.sync.get([tabTimeObjectKey, lastActiveTabKey], function(
          result
        ) {
          let lastActiveTabString = result[lastActiveTabKey];
          let tabTimeObjectString = result[tabTimeObjectKey];

          //console.log("backgroundjs,getresult");

          //console.log(result);
          tabTimeObject = {};

          if (tabTimeObjectString != null) {
            try {
              tabTimeObject = JSON.parse(tabTimeObjectString);
            } catch (e) {
              console.log("exception" + e);
            }
          }

          lastActiveTab = {};
          if (lastActiveTabString != null) {
            try {
              lastActiveTab = JSON.parse(lastActiveTabString);
            } catch (e) {
              console.log("exception" + e);
            }
          }

          if (
            lastActiveTab.hasOwnProperty("url") &&
            lastActiveTab.hasOwnProperty("lastDateVal")
          ) {
            let lastUrl = lastActiveTab["url"];
            let currentDateVal_ = Date.now();
            let passedSecond =
              (currentDateVal_ - lastActiveTab["lastDateVal"]) * 0.001;

            if (tabTimeObject.hasOwnProperty(lastUrl)) {
              let lastUrlObjectInfo = tabTimeObject[lastUrl];

              if (lastUrlObjectInfo.hasOwnProperty("trackedSeconds")) {
                lastUrlObjectInfo["trackedSeconds"] =
                  lastUrlObjectInfo["trackedSeconds"] + passedSecond;
              } else {
                lastUrlObjectInfo["trackedSeconds"] = passedSecond;
              }
              lastUrlObjectInfo["lastDateVal"] = currentDateVal_;
              //lastUrlObjectInfo["favicon"] = favicon;
            } else {
              let newUrlInfo = {
                favicon: favicon,
                url: lastUrl,
                trackedSeconds: passedSecond,
                lastDateVal: currentDateVal_,
                hashtag: "",
                liked: false
              };
              tabTimeObject[lastUrl] = newUrlInfo;
            }
          }

          let currentDateVal = Date.now();
          let lastTabInfo = {
            url: hostName,
            lastDateVal: currentDateVal
          };

          if (!isWindowActive) {
            lastTabInfo = {};
          }
          let newLastTabObject = {};
          newLastTabObject[lastActiveTabKey] = JSON.stringify(lastTabInfo);

          chrome.storage.sync.set(newLastTabObject, function() {
            console.log("lastActiveTab stored:" + hostName);

            const tabTimeObjectString = JSON.stringify(tabTimeObject);
            let newTabTimeObject = {};

            newTabTimeObject[tabTimeObjectKey] = tabTimeObjectString;
            chrome.storage.sync.set(newTabTimeObject, function() {});
          });
        });
      }
    }
  );
}

function onTabTrack(activeTabInfo) {
  let tabId = activeTabInfo.tabId;
  let windowId = activeTabInfo.windowId;

  processTabChange(true);
}

function startDbTimer() {
  setInterval(dbRefreshTimer, 30 * 60 * 1000);
}

function dbRefreshTimer() {
  console.log("----------------------------Refresh------------------------");
  chrome.storage.sync.get("tabTimesObject", function(dataCont) {
    let dataString = dataCont["tabTimesObject"];
    if (dataString == null) {
      return;
    }
    try {
      let data = JSON.parse(dataString);

      let entries = [];

      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          entries.push(data[key]);
        }
      }

      var req = new XMLHttpRequest();
      const baseUrl = "https://toolmark.io/userInfoUpdate";

      req.open("POST", baseUrl, true);
      req.setRequestHeader("Content-Type", "application/json");
      req.send(JSON.stringify(data));

      req.onreadystatechange = function() {
        // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
          //console.log("Got response 200!");
        }
      };
    } catch (e) {
      const message = "loading the timeObject went wrong " + e.toString();
      //console.log(message);

      errorMessage.innerHTML = message;
      errorMessage.innerHTML = dataString;
    }
  });
}

startDbTimer();

chrome.tabs.onActivated.addListener(onTabTrack);
chrome.tabs.onUpdated.addListener(onTabTrack);
chrome.tabs.onRemoved.addListener(onTabTrack);
chrome.tabs.onReplaced.addListener(onTabTrack);

//test
